// "use strict";
const Student = require("../models/studentForm");
const mongoose = require("mongoose");
// const config = require("../config/db");
var _ = require("lodash");
// const jwt = require("jsonwebtoken");
var Client = require('node-rest-client').Client;
var client = new Client();
const sequenceNumber = require("../models/sequenceNumber");
const fs = require('fs');
const http = require('http');
module.exports = {
    async addStudent(req, res) {
        let respObj = {
            IsSuccess: false,
            Message: "OK",
            Data: null,
            Token: null,
        };
        try {
            let result;
            let checkEmail = await Student.findOne({
                registration: req.body.registration,
                //isDeleted: false,
                //rejected: false
            });
            if (checkEmail) {
                if (checkEmail.rejected || checkEmail.isDeleted) {
                    if (checkEmail.offerLetterUrl && !checkEmail.offerLetterRejected) {
                        const result = await Student.findOneAndUpdate({ registration: req.body.registration },
                            {
                                $set: {
                                    viedoUrl: req.body.viedoUrl,
                                    accepted: false,
                                    rejected: false,
                                    isDeleted: false
                                }
                            }, {
                            new: true
                        })
                        result
                            ? res.status(200).send({
                                IsSuccess: true,
                                message: "Data upload Succefully",
                                IsOfferLetter: false

                            })
                            : res.status(422).send({
                                IsSuccess: false, message: "fail to upload"
                            });
                    }
                    else {
                        const result = await Student.findOneAndUpdate({ registration: req.body.registration },
                            {
                                $set: {
                                    viedoUrl: req.body.viedoUrl,
                                    accepted: false,
                                    rejected: false,
                                    isDeleted: false
                                }
                            }, {
                            new: true
                        })
                        result
                            ? res.status(200).send({
                                IsSuccess: true,
                                message: "Data upload Succefully",
                                IsOfferLetter: true

                            })
                            : res.status(422).send({
                                IsSuccess: false, message: "fail to upload"
                            });
                    }


                }
                else {
                    res.status(200).send({
                        IsSuccess: false,
                        message: "You have already uploaded your video",
                        code: 422
                    })
                }

            } else {
                // req.body.serialNumber = 

                // const result = await newStudent.save();
                // console.log("result########################## ",result._id);
                // let numberData = await sequenceNumber.find().then(async function(data){
                //     console.log("data................. ",data[0].higestNumber);
                //     const IncNumber = data[0].higestNumber + 1;
                //     req.body.serialNumber = IncNumber;
                //     let newStudent = new Student(req.body);
                //     const result = await newStudent.save();
                //     await sequenceNumber.updateOne({
                //         higestNumber: IncNumber
                //     })

                // });
                // console.log("numberData............... ", numberData)


                Student.findOne({}, {}, { sort: { 'serialNumber': -1 } }, async function (err, data) {
                    const IncNumber = data.serialNumber + 1;
                    req.body.serialNumber = IncNumber;
                    let newStudent = new Student(req.body);
                    const result = await newStudent.save();
                    result
                        ? res.status(200).send({
                            IsSuccess: true,
                            IsOfferLetter: true,
                            Data: result,
                            message: "Data upload Succefully",

                        })
                        : res
                            .status(422)
                            .send({ IsSuccess: false, message: "fail to upload" });

                });


                // if(result){
                //     var args = {
                //         headers: { "Content-Type": "text/plain" },
                //         parameters:
                //         { method: 'SendMessage',
                //         send_to: req.body.mobile,
                //         msg: 'Dear student, your video has been successfully uploaded and accepted. Kindly share your offer letter for the further needful. -LPU',
                //         msg_type: 'TEXT',
                //         userid: process.env.USER_ID,
                //         auth_scheme: 'plain',
                //         password: process.env.G_PASS,
                //         format: 'text',
                //         v: '1.1'
                //         } 
                //     };
                //     client.post("http://enterprise.smsgupshup.com/GatewayAPI/rest", args, function (data, response) {
                //         // parsed response body as js object
                //         //console.log("data", data);
                //         // raw response
                //         //console.log(response);
                //         // if(response){
                //         //     res.status(200).send({
                //         //         message: "get successfully",
                //         //         IsSuccess: true,
                //         //         //Data: data,
                //         //     });
                //         // }
                //         }).on('error', function (err) {
                //         console.log('something went wrong on the request', err.request.options);
                //     });
                // }


            }

        } catch (err) {
            res.status(400).send({ IsSuccess: false, message: err })
        }
    },
    async getStudentsList(req, res) {

        try {
            if (req.query.isAccepted == '' || req.query.isRejected == '') {
                const count = await Student.countDocuments({
                    "$and": [{ isDeleted: false }]
                })
                const pageOptions = {
                    page: parseInt(req.query.pageNo - 1, 10) || 0,
                    limit: parseInt(req.query.pageSize, 10) || 50
                }
                const countFilter = await Student.countDocuments({
                    "$or": [
                        { name: { '$regex': req.query.searchParam, '$options': 'i' } },
                        { registration: { '$regex': req.query.searchParam, '$options': 'i' } },
                        { email: { '$regex': req.query.searchParam, '$options': 'i' } },
                    ],
                    "$and": [{ isDeleted: false }]
                })
                const result = await Student.find({
                    "$or": [
                        { name: { '$regex': req.query.searchParam, '$options': 'i' } },
                        { registration: { '$regex': req.query.searchParam, '$options': 'i' } },
                        { email: { '$regex': req.query.searchParam, '$options': 'i' } },


                    ],
                    "$and": [{ isDeleted: false }]
                }).skip(pageOptions.page * pageOptions.limit)
                    .limit(pageOptions.limit).exec(function (err, doc) {
                        if (err) {
                            res.status(422).send({
                                message: "not getting",
                                IsSuccess: false
                            })
                        };
                        res.status(200).send({
                            message: "get suucessfully",
                            IsSuccess: true,
                            Data: doc,
                            count: count,
                            countFilter: countFilter
                        });
                    });

            } else {
                const count = await Student.countDocuments({
                    "$and": [{ isDeleted: false }]
                })
                const pageOptions = {
                    page: parseInt(req.query.pageNo - 1, 10) || 0,
                    limit: parseInt(req.query.pageSize, 10) || 50
                }
                const countFilter = await Student.countDocuments({
                    "$or": [
                        { name: { '$regex': req.query.searchParam, '$options': 'i' } },
                        { registration: { '$regex': req.query.searchParam, '$options': 'i' } },
                        { email: { '$regex': req.query.searchParam, '$options': 'i' } },
                    ], "$and": [{ accepted: req.query.isAccepted },
                    { rejected: req.query.isRejected }, { isDeleted: false }]
                })
                const result = await Student.find({
                    "$or": [
                        { name: { '$regex': req.query.searchParam, '$options': 'i' } },
                        { registration: { '$regex': req.query.searchParam, '$options': 'i' } },
                        { email: { '$regex': req.query.searchParam, '$options': 'i' } },


                    ], "$and": [{ accepted: req.query.isAccepted },
                    { rejected: req.query.isRejected }, { isDeleted: false }
                    ]
                }).skip(pageOptions.page * pageOptions.limit)
                    .limit(pageOptions.limit).exec(function (err, doc) {
                        if (err) {
                            res.status(422).send({
                                message: "not getting",
                                IsSuccess: false
                            })
                        };
                        res.status(200).send({
                            message: "get suucessfully",
                            IsSuccess: true,
                            Data: doc,
                            count: count,
                            countFilter: countFilter
                        });
                    });

            }
            // result?res.status(200).send({
            //     message:"get suucessfully",
            //     IsSuccess:true,
            //     Data:result
            // }):
            // res.status(422).send({
            //     message:"not getting",
            //     IsSuccess:false
            // })

        } catch (err) {
            res.send(err)
        }
    },
    async updateStatus(req, res) {
        try {
            const student = await Student.findById({ _id: req.params.id })

            if (req.body.rejected == false) {

                const result = await Student.findOneAndUpdate({ _id: req.params.id },
                    req.body, {
                    new: true
                });
                if (result) {
                    var args = {
                        headers: { "Content-Type": "text/plain" },
                        parameters:
                        {
                            method: 'SendMessage',
                            send_to: student.mobile,
                            msg: 'Dear student, your video has been successfully uploaded and accepted. Kindly share your offer letter for the further needful. -LPU',
                            msg_type: 'TEXT',
                            userid: process.env.USER_ID,
                            auth_scheme: 'plain',
                            password: process.env.G_PASS,
                            format: 'text',
                            v: '1.1'
                        }
                    };
                    client.post("http://enterprise.smsgupshup.com/GatewayAPI/rest", args, function (data, response) {
                        // parsed response body as js object
                        // console.log("data", data);
                        // raw response
                        // console.log(response);
                        // if(response){
                        //     res.status(200).send({
                        //         message: "sms sent",
                        //         IsSuccess: true,
                        //         //Data: data,
                        //     });
                        //     }
                    }).on('error', function (err) {
                        console.log('something went wrong on the request', err.request.options);
                    });
                }
                result ? res.status(200).send({
                    message: "update status",
                    IsSuccess: true,
                    Data: result
                }) :
                    res.status(422).send({
                        message: "not updated",
                        IsSuccess: false
                    })



            } else if (req.body.rejectMessage == "Reshoot") {

                const result = await Student.findOneAndUpdate({ _id: req.params.id },
                    req.body, {
                    new: true
                });
                if (result) {
                    var args = {
                        headers: { "Content-Type": "text/plain" },
                        parameters:
                        {
                            method: 'SendMessage',
                            send_to: student.mobile,
                            msg: 'Dear student, your video is not accepted as it does not meet prescribed specifications. Kindly reupload new video. -LPU',
                            msg_type: 'TEXT',
                            userid: process.env.USER_ID,
                            auth_scheme: 'plain',
                            password: process.env.G_PASS,
                            format: 'text',
                            v: '1.1'
                        }
                    };
                    client.post("http://enterprise.smsgupshup.com/GatewayAPI/rest", args, function (data, response) {
                        // parsed response body as js object
                        // console.log("data", data);
                        // raw response
                        // console.log("res",response);
                        // if(response){
                        // res.status(200).send({
                        //     message: "sms sent",
                        //     IsSuccess: true,
                        //     //Data: data,
                        // });
                        // }
                    }).on('error', function (err) {
                        console.log('something went wrong on the request', err.request.options);
                    });
                }
                result ? res.status(200).send({
                    message: "update status",
                    IsSuccess: true,
                    Data: result
                }) :
                    res.status(422).send({
                        message: "not updated",
                        IsSuccess: false
                    })



            } else if (req.body.rejectMessage == "Can be improved") {

                const result = await Student.findOneAndUpdate({ _id: req.params.id },
                    req.body, {
                    new: true
                });
                result ? res.status(200).send({
                    message: "update status",
                    IsSuccess: true,
                    Data: result
                }) :
                    res.status(422).send({
                        message: "not updated",
                        IsSuccess: false
                    })

            }
            else {
                res.status(422).send({
                    message: "Something went wrong!",
                    IsSuccess: false
                })

            }
        } catch (err) {
            res.send(err)

        }
    },

    async recordExist(req, res) {
        try {
            const result = await Student.findOne({
                registration: req.query.registration,
                isDeleted: false
            });
            if (result) {
                if (result.accepted && !result.rejected) {
                    if (result.offerLetterAccepted && !result.offerLetterRejected) {
                        res.status(200).send({
                            message: "Your video and offer letter has been selected !",
                            IsSuccess: false,
                            IsOfferLetter: false
                        })
                    }
                    else if (!result.offerLetterAccepted && !result.offerLetterRejected && result.offerLetterUrl) {
                        res.status(200).send({
                            message: "Your video has been selected and offer letter is uploaded",
                            IsSuccess: false,
                            IsOfferLetter: false
                        })
                    }
                    else {
                        res.status(200).send({
                            message: "Your video has been selected. Please upload offer letter (For Placed Student Only)",
                            IsSuccess: false,
                            IsOfferLetter: true,
                            Data: result
                        })
                    }

                }
                else if (!result.accepted && !result.rejected) {

                    // res.status(200).send({
                    //     message: "You have already uploaded your video",
                    //     IsSuccess: false})
                    if (!result.offerLetterAccepted && !result.offerLetterRejected && result.offerLetterUrl) {
                        res.status(200).send({
                            message: "Your video and offer letter is uploaded",
                            IsSuccess: false,
                            IsOfferLetter: false
                        })
                    }
                    else if (result.offerLetterAccepted && !result.offerLetterRejected && result.offerLetterUrl) {
                        res.status(200).send({
                            message: "Your video is uploaded and offer letter is accepted",
                            IsSuccess: false,
                            IsOfferLetter: false
                        })
                    }
                    else {
                        res.status(200).send({
                            message: "Your video has been uploaded. Please upload offer letter (For Placed Student Only)",
                            IsSuccess: false,
                            IsOfferLetter: true,
                            Data: result
                        })
                    }
                }
                else {
                    // res.status(200).send({
                    //     message: "New record",
                    //     IsSuccess: true,
                    //     Data: result
                    // })
                    if (!result.offerLetterAccepted && !result.offerLetterRejected && result.offerLetterUrl) {
                        res.status(200).send({
                            message: "New video , offer letter uploaded",
                            IsSuccess: true,
                            IsOfferLetter: false
                        })
                    }
                    else if (result.offerLetterAccepted && !result.offerLetterRejected && result.offerLetterUrl) {
                        res.status(200).send({
                            message: "New video , offer letter uploaded and accepted",
                            IsSuccess: true,
                            IsOfferLetter: false
                        })
                    }
                    else {
                        res.status(200).send({
                            message: "New record",
                            IsSuccess: true,
                            IsOfferLetter: true,
                            Data: result
                        })
                    }
                }
            }
            else {
                res.status(200).send({
                    message: "New record",
                    IsSuccess: true,
                    IsOfferLetter: true
                })
            }



        } catch (err) {
            res.send(err)

        }
    },
    async sendMessage(req, res) {
        try {
            var args = {
                headers: { "Content-Type": "text/plain" },
                parameters:
                {
                    method: 'SendMessage',
                    send_to: '8288820215',
                    msg: 'Dear student, your video is not accepted as it does not meet prescribed specifications. Kindly reupload new video. -LPU',
                    msg_type: 'TEXT',
                    userid: process.env.USER_ID,
                    auth_scheme: 'plain',
                    password: process.env.G_PASS,
                    format: 'text',
                    v: '1.1'
                }
            };
            client.post("http://enterprise.smsgupshup.com/GatewayAPI/rest", args, function (data, response) {
                // parsed response body as js object
                //console.log("data", data);
                // raw response
                //console.log(response);
                if (response) {
                    res.status(200).send({
                        message: "get successfully",
                        IsSuccess: true,
                        //Data: data,
                    });
                }
            }).on('error', function (err) {
                console.log('something went wrong on the request', err.request.options);
            });

        } catch (err) {
            res.send(err)

        }
    },
    async downloadRecord(req, res) {
        let resObj = {
            IsSuccess: false,
            Data: null,
            Message: true
        }
        try {
            // const result = await Student.find({
            //     accepted: true,
            //     isDeleted: false
            // })
            // .skip(1).limit(2);

            let result = await Student.aggregate([

                //{$sort: {...}}

                {
                    $match: {
                        // 'accepted': true,
                        'isDeleted': false
                    }
                },
                {
                    $facet: {

                        "stage1": [{ "$group": { _id: null, count: { $sum: 1 } } }],

                        "stage2": [{ "$skip": parseInt(req.query.skip) }, { "$limit": parseInt(req.query.limit) }]

                    }
                },
                { $unwind: "$stage1", $unwind: "$stage2" },
                {
                    $project: {
                        //count: "$stage1.count",
                        SerialNumber: "$stage2.serialNumber",
                        Name: "$stage2.name", Registration: "$stage2.registration",
                        Email: "$stage2.email", Mobile: "$stage2.mobile",
                        Description: "$stage2.description", video_Url: "$stage2.viedoUrl",
                        Video_Accepted: "$stage2.accepted", Video_Rejected: "$stage2.rejected"
                    }
                }

            ]);
            // if (result) {
            //     fs.mkdir("C:/videos", { recursive: true }, function (err) {
            //         if (err) {
            //             console.log(err)
            //         } else {
            //             console.log("New directory successfully created.")
            //             result.forEach((data, i) => {
            //                 setTimeout(() => {
            //                     let regNo = data.Registration;
            //                     let Vlink = data.video_Url.split('https')[1];
            //                     // console.log("http://${Vlink}.........", `http${Vlink}`);
            //                     const file = fs.createWriteStream(`C:/videos/${regNo}.mp4`);
            //                     const request = http.get(`http${Vlink}`, function (response) {
            //                         response.pipe(file);
            //                     });
            //                 }, 5000);
            //             })
            //         }
            //     })

            // }

            result ? res.status(200).send({
                message: "data fetched",
                IsSuccess: true,
                Data: result
            }) :
                res.status(422).send({
                    message: "data not fetched",
                    IsSuccess: false
                })

        } catch (err) {
            console.log("err......... ", err)
            res.send({ message: "Something is wrong", Data: err })
        }
    },
    async getAcceptedRecordCount(req, res) {
        let resObj = {
            IsSuccess: false,
            Data: null,
            Message: true
        }
        try {
            const result = await Student.find({
                accepted: true,
                isDeleted: false
            }).count();

            result ? res.status(200).send({
                message: "data fetched",
                IsSuccess: true,
                Data: result
            }) :
                res.status(422).send({
                    message: "data not fetched",
                    IsSuccess: false
                })

        } catch (err) {
            res.send({ message: "Something is wrong", Data: err })
        }
    },
    async allRecordCount(req, res) {
        let resObj = {
            IsSuccess: false,
            Data: null,
            Message: true
        }
        try {
            const result = await Student.find({
                isDeleted: false
            }).count();

            result ? res.status(200).send({
                message: "data fetched",
                IsSuccess: true,
                Data: result
            }) :
                res.status(422).send({
                    message: "data not fetched",
                    IsSuccess: false
                })

        } catch (err) {
            res.send({ message: "Something is wrong", Data: err })
        }
    },
    async updateOfferLetter(req, res) {
        let respObj = {
            IsSuccess: false,
            Message: "OK",
            Data: null,
        };
        try {
            let student = await Student.findOne({
                registration: req.body.registration,
                isDeleted: false
            });
            if (student) {
                if (!student.offerLetterAccepted && student.offerLetterRejected) {
                    const result = await Student.findOneAndUpdate({
                        registration: req.body.registration,
                        isDeleted: false
                    },
                        {
                            $set: {
                                offerLetterUrl: req.body.offerLetterUrl,
                                offerLetterAccepted: false,
                                offerLetterRejected: false
                            }
                        }, {
                        new: true
                    })
                    result
                        ? res.status(200).send({
                            IsSuccess: true,
                            message: "Data uploaded successfully",

                        })
                        : res.status(422).send({
                            IsSuccess: false, message: "Failed to upload"
                        });
                }
                else if (!student.offerLetterUrl) {
                    const result = await Student.findOneAndUpdate({
                        registration: req.body.registration,
                        isDeleted: false
                    },
                        { $set: { offerLetterUrl: req.body.offerLetterUrl } }, {
                        new: true
                    })
                    result
                        ? res.status(200).send({
                            IsSuccess: true,
                            message: "Data uploaded successfully",

                        })
                        : res.status(422).send({
                            IsSuccess: false, message: "Failed to upload"
                        });
                }
                else {
                    res.status(200).send({
                        IsSuccess: false,
                        message: "You have already uploaded your offer letter",
                        code: 422
                    })
                }

            }
            else {
                res.status(200).send({
                    IsSuccess: false,
                    message: "Please upload video first",
                    code: 422
                })
            }

        } catch (err) {
            res.status(400).send({ IsSuccess: false, message: err })
        }
    },

    async updateOfferLetterStatus(req, res) {
        try {
            const student = await Student.findById({ _id: req.params.id })

            if (req.body.offerLetterRejected == false) {

                const result = await Student.findOneAndUpdate({ _id: req.params.id },
                    { $set: { offerLetterAccepted: true } }, {
                    new: true
                });
                // if(result){
                //     var args = {
                //         headers: { "Content-Type": "text/plain" },
                //         parameters:
                //         { method: 'SendMessage',
                //         send_to: student.mobile,
                //         msg: 'Dear student, your video and offer letter has been successfully uploaded and accepted. -LPU',
                //         msg_type: 'TEXT',
                //         userid: process.env.USER_ID,
                //         auth_scheme: 'plain',
                //         password: process.env.G_PASS,
                //         format: 'text',
                //         v: '1.1'
                //         } 
                //     };
                //     client.post("http://enterprise.smsgupshup.com/GatewayAPI/rest", args, function (data, response) {
                //         // parsed response body as js object
                //         // console.log("data", data);
                //         // raw response
                //         // console.log(response);
                //     // if(response){
                //     //     res.status(200).send({
                //     //         message: "sms sent",
                //     //         IsSuccess: true,
                //     //         //Data: data,
                //     //     });
                //     //     }
                //     }).on('error', function (err) {
                //         console.log('something went wrong on the request', err.request.options);
                //     });
                // }
                result ? res.status(200).send({
                    message: "update status",
                    IsSuccess: true,
                    Data: result
                }) :
                    res.status(422).send({
                        message: "not updated",
                        IsSuccess: false
                    })



            } else {

                const result = await Student.findOneAndUpdate({ _id: req.params.id },
                    { $set: { offerLetterRejected: true } }, {
                    new: true
                });
                // if(result){
                //     var args = {
                //         headers: { "Content-Type": "text/plain" },
                //         parameters:
                //         { method: 'SendMessage',
                //         send_to: student.mobile,
                //         msg: 'Dear student, your video is not accepted as it does not meet prescribed specifications. Kindly reupload new video. -LPU',
                //         msg_type: 'TEXT',
                //         userid: process.env.USER_ID,
                //         auth_scheme: 'plain',
                //         password: process.env.G_PASS,
                //         format: 'text',
                //         v: '1.1'
                //         } 
                //     };
                //     client.post("http://enterprise.smsgupshup.com/GatewayAPI/rest", args, function (data, response) {
                //         // parsed response body as js object
                //         // console.log("data", data);
                //         // raw response
                //         // console.log("res",response);
                //         // if(response){
                //         // res.status(200).send({
                //         //     message: "sms sent",
                //         //     IsSuccess: true,
                //         //     //Data: data,
                //         // });
                //         // }
                //     }).on('error', function (err) {
                //         console.log('something went wrong on the request', err.request.options);
                //     });
                // }
                result ? res.status(200).send({
                    message: "update status",
                    IsSuccess: true,
                    Data: result
                }) :
                    res.status(422).send({
                        message: "not updated",
                        IsSuccess: false
                    })



            }
        } catch (err) {
            res.send(err)

        }
    },
    async getAcceptedRecord(req, res) {
        let resObj = {
            IsSuccess: false,
            Data: null,
            Message: true,
            Count: null
        }
        try {
            const Count = await Student.find({
                accepted: true,
                isDeleted: false
            }).count();
            const pageOptions = {
                page: parseInt(req.query.pageNo - 1, 10) || 0,
                limit: parseInt(req.query.pageSize, 10) || 50
            }
            const result = await Student.find({
                accepted: true,
                isDeleted: false
            })
                .skip(pageOptions.page * pageOptions.limit)
                .limit(pageOptions.limit)
            result ? res.status(200).send({
                message: "data fetched",
                IsSuccess: true,
                Data: result,
                Count: Count
            }) :
                res.status(422).send({
                    message: "data not fetched",
                    IsSuccess: false
                })

        } catch (err) {
            res.send({ message: "Something is wrong", Data: err })
        }
    },
    async deleteStudentRecord(req, res) {
        let resObj = {
            IsSuccess: false,
            Data: null,
            Message: true
        }
        try {
            const result = await Student.findOneAndUpdate({ registration: req.body.registration },
                {
                    $set: {
                        isDeleted: true,
                        rejected: true,
                        accepted: false
                    }
                }, {
                new: true
            })
            result ? res.status(200).send({
                message: "data deleted",
                IsSuccess: true,
                Data: result
            }) :
                res.status(422).send({
                    message: "error",
                    IsSuccess: false
                })

        } catch (err) {
            res.send({ message: "Something is wrong", Data: err })
        }
    },

    async addAutoIncrementNumber(req, res) {
        let resObj = {
            IsSuccess: false,
            Data: null,
            Message: true
        }
        try {
            const result = await Student.find()
                .then(async function (data) {
                    data.forEach(async (element, i) => {
                        let updateInc = await Student.findByIdAndUpdate({ _id: element._id }, {
                            $set: {
                                serialNumber: i + 1
                            }
                        })
                    })
                    if (data.length - 1) {
                        updateInc ? res.status(200).send({
                            message: "data updated",
                            IsSuccess: true,
                            Data: updateInc
                        }) :
                            res.status(422).send({
                                message: "error",
                                IsSuccess: false
                            })
                    }
                })


        } catch (err) {
            res.send({ message: "Something is wrong", Data: err })
        }
    },
}
