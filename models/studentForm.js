"use strict";
const mongoose = require('mongoose');
// const config = require('../config/db');
const Schema = mongoose.Schema;

const Student = mongoose.Schema({
    name: {
        type: String
    },
    serialNumber: {
        type: Number,
        default: 1
    },
    email: {
        type: String,
        required: [true, 'Email is required']
    },
    registration: {
        type: String,
        required: [true, 'registration number is required']
    },
    mobile: {
        type: Number,
    },
    description: {
        type: String
    },
    viedoUrl: {
        type: String
    },
    offerLetterUrl: {
        type: String
    },
    created_At: {
        type: Date
    },
    accepted: {
        type: Boolean,
        default: false
    },
    rejected: {
        type: Boolean,
        default: false
    },
    offerLetterAccepted: {
        type: Boolean,
        default: false
    },
    offerLetterRejected: {
        type: Boolean,
        default: false
    },
    rejectMessage: {
        type: String
    },
    isDeleted:{
        type:Boolean,
        default:false 
    }
});

const studentUser = module.exports = mongoose.model('studentUser', Student);



