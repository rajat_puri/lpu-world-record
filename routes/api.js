// const jwt = require('jsonwebtoken');
const express = require("express");
let router = express.Router();
const studentController = require("../controllers/studentForm");
const multer = require('multer')


// api routes
const upload = multer({
    dest: '../tmp/videos'
})
// console.log("upload.... ",upload)
router.post("/upload", upload.single('file'), studentController.addStudent);
router.get("/getStudentsList", studentController.getStudentsList);
router.put("/updateStatus/:id", studentController.updateStatus);
router.get("/recordExists", studentController.recordExist);
router.post("/sendMessage", studentController.sendMessage);
router.post("/uploadOfferLetter", upload.single('file'), studentController.updateOfferLetter);
router.put("/updateOfferLetterStatus/:id", studentController.updateOfferLetterStatus);
router.get("/getStudentsAcceptedRecords", studentController.getAcceptedRecord);
router.put("/deleteRecord/:id", studentController.deleteStudentRecord);



router.get("/downloadRecord", studentController.downloadRecord);
router.get("/getAcceptedRecordCount", studentController.getAcceptedRecordCount);
router.get("/allRecordCount", studentController.allRecordCount);

router.get("/addAutoIncrementNumber", studentController.addAutoIncrementNumber);
module.exports = router;