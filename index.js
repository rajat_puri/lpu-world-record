require("dotenv").config();
let express = require("express");
let bodyParser = require("body-parser");
let http = require("http");
let path = require("path");
let hbs = require("express-handlebars");
let cors = require("cors");
// let nLog = require("noogger");
const Handlebars = require("handlebars");

// Init App
var app = express();
var server = http.createServer(app);
let mongoose = require("mongoose");

//logger configurations
// var nlogParams = {
//   consoleOutput: true,
//   consoleOutputLevel: "DEBUG",
//   dateTimeFormat: "DD-MM-YYYY HH:mm:ss",
//   fileNameDateFormat: "YYYY-MM-DD",
//   fileNamePrefix: "cl-",
//   outputPath: "logs/"
// };
// nLog.init(nlogParams);

//Use CORS
app.use(cors());



// Use body-parser to get POST requests for API use
app.use(
  bodyParser.json({
    limit: "50mb"
  })
);

app.use("/assets", express.static(path.join(__dirname, "assets")));

// Handle 500
app.use(function (error, req, res, next) {
  if (500 === err.status) {
    res.render("500", {
      layout: null
    });
  }
});

// Routes setup
let publicRoutes = require("./routes/api");

app.use("/", publicRoutes);

// View Engine
app.set("views", path.join(__dirname, "html"));
app.engine(
  "hbs",
  hbs({
    extname: "hbs",
    defaultLayout: "studentForm",
    layoutsDir: __dirname + "/html/",
    // partialsDir: __dirname + "/views/partials/"
  })
);
// app.engine(
//   "hbs",
//   hbs({
//     extname: "hbs",
//     defaultLayout: "studentForm",
//     layoutsDir: __dirname + "/html/",
//     // partialsDir: __dirname + "/views/partials/"
//   })
// );


let hbsR = hbs.create({});
hbsR.handlebars.registerHelper("apiBaseURL", () => {
  // if (process.env.NODE_ENV === "prod")
  //   return process.env.PROD_API_MASTERUNION_ORG;
  return process.env.DEV_API;
});


app.get('/registration', function (req, res) {
  //res.redirect('https://placementnetwork.lpu.in')
  res.render('studentForm', {
    title: 'Lpu word record',
    description:'Lpu word record video',
    metaData: {
      img: "https://cdn.mastersunion.org/assets/LPU/logo.png",
      url: "https://lpu-world-record.mastersunion.org/",
    },
  })
})
app.get('/', function (req, res) {
  //res.redirect('https://placementnetwork.lpu.in/publicDetails')
  res.render('publicPlatform', {
    layout: 'publicPlatform',
    title: 'Lpu word record',
    description:'Lpu word record video',
    metaData: {
      img: "https://cdn.mastersunion.org/assets/LPU/logo.png",
      url: "https://lpu-world-record.mastersunion.org/",
    },

  })
})

app.get('/contactUs', function (req, res){
  //res.redirect('https://placementnetwork.lpu.in')
  res.render('publicPlatform', {
    layout: 'contact',
    title: 'Lpu word record:Contact Details',
    description:'Contact Details',
    metaData: {
      img: "https://cdn.mastersunion.org/assets/LPU/logo.png",
      url: "https://lpu-world-record.mastersunion.org/",
    },
  })
})
app.set("view engine", "hbs");

app.set("port", process.env.PORT || 80);

server.listen(app.get("port"), function () {
  console.log("Server started on port " + app.get("port"));
});
// connect to mongodb
mongoose.connect(process.env.MONGO_DB_CONN_STRING, { useNewUrlParser: true },
  function (err) {
    if (err) console.log(err)
    else
      console.log("connected")
  });
