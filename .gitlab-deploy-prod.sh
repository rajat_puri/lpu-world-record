#!/bin/bash

# Get servers list
set -f
string=$PROD_SERVER
array=(${string//,/ })

# iterate servers for deploy and pull latest code
for i in "${!array[@]}"; do
  echo "deploying to ${array[i]}"
  ssh ubuntu@${array[i]} "cd /home/ubuntu/lpu/lpu-world-record && git pull origin master && npm i && forever restart index.js" 
done

